<?php

if (!defined('_PS_VERSION_'))
    exit();

class Remember_My_Social_Network extends Module
{
    public function __construct()
    {
        $this->name = 'remember_my_social_network';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Aleso';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Remember my social network', 'remember_my_social_network');
        $this->description = $this->l('Remember visitors to ckeck your social network.', 'remember_my_social_network');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?', 'remember_my_social_network');
    }
    
	
	public function install()
	{
    	if (Shop::isFeatureActive())
        	Shop::setContext(Shop::CONTEXT_ALL);
	
    	return parent::install() &&
        	$this->registerHook('displayHome') && Configuration::updateValue('SOCIAL_NETWORK_URL', 'https://www.instagram.com/a_l_e_s_o/')
        									   && Configuration::updateValue('SOCIAL_NETWORK_TIME', 10)        									           																	   && Configuration::updateValue('SOCIAL_NETWORK_PHRASE', 'Visita mis redes sociales');

	}
	
	public function uninstall()
	{
    	if (!parent::uninstall() || !Configuration::deleteByName('SOCIAL_NETWORK_URL') 
    							 || !Configuration::deleteByName('SOCIAL_NETWORK_TIME')	 
    							 || !Configuration::deleteByName('SOCIAL_NETWORK_PHRASE')
    	   ){ 
    	   
    		return false;
    	
    		}else{
    		
    			return true;
    		}
		} 
	
		
	
	
	public function hookDisplayHome($params)
	{             	 
		if (!isset($_COOKIE['Social_Reminder']))
		{
			$url = Configuration::get('SOCIAL_NETWORK_URL');
			$time = Configuration::get('SOCIAL_NETWORK_TIME');
			$phrase = Configuration::get('SOCIAL_NETWORK_PHRASE');
			//setcookie("Social_Reminder", 'Social_Reminder', time()+10);  /* expire in ten seconds, only for testing */
			setcookie("Social_Reminder", 'Aleso-2019', time()+60*60*24*$time ); 
    		$this->smarty->assign(array('social_url' => $url, 'social_phrase' => $phrase));
    		return $this->display(__FILE__, 'remember_my_social_network.tpl');
    		
    	 }else{
    	 
    		return false;
    	 }	

	}
	
	
	public function getContent()
	{ 	
	    $url = Configuration::get('SOCIAL_NETWORK_URL');
	    $time = Configuration::get('SOCIAL_NETWORK_TIME');
	    $phrase = Configuration::get('SOCIAL_NETWORK_PHRASE');
	    
	   	$this->smarty->assign(array('url' => $url,
	   								'time' => $time,
	   	 							'phrase' => $phrase));
	   	 							

	  	if (Tools::isSubmit('submitRemember')) 
	  	{
	  	    $newUrl = strval(Tools::getValue('new_url'));
	  	    $newTime = strval(Tools::getValue('new_time'));
	  	    $newPhrase = strval(Tools::getValue('new_phrase'));



        	if (isset($newUrl) && $newUrl!==$url)
        	{
            	Configuration::updateValue('SOCIAL_NETWORK_URL', $newUrl);
			  	 $this->smarty->assign('url', 'Url UPDATED OK!' );
        	}
        	
        	if (isset($newTime) && $newTime!==$time)
        	{
        	   Configuration::updateValue('SOCIAL_NETWORK_TIME', $newTime);
        	   $this->smarty->assign('time', 'Time UPDATED OK!' );
        	}
        	
        	if (isset($newPhrase) && $newPhrase!==$phrase)
        	{
        	   Configuration::updateValue('SOCIAL_NETWORK_PHRASE', $newPhrase);
        	   $this->smarty->assign('phrase', 'Phrase UPDATED OK!' );
        	}
    	}

    	return $this->display(__FILE__,'configure.tpl');
	}	
	

}
